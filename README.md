# Repo History
Everytime I, Alison, get a new computer I forget a step in the setup process. Here's my documentation.

## Notes
This repository started as "How to Setup a Mac Development Environment" because I was buying, or setting up for others, Macintoshs. I will not be changing the name of the repo at this time to assist in keeping it findable for others.

I've expanded this repo into other computer OSes and general tech stack environments so I have all I need in one place. 
* [Ubuntu 22.04 on my Framework Laptop](ubuntu-22.04_jammy_framework.md)
* [Mac OS](mac-os_thru_2021.md)
* [AWS](aws.md)

I've started another project in it for easy sharing. Note, searching Github gets you way more content.
* [De-Google Your Life](de-google.md)

### Repo History 
* This Gitlab version is the canonical version. This [repository moved from my Github.com](https://github.com/alison985/setting-up-mac-dev-environment) account. It had 8 stars and 2 forks when I moved it. :proud of ability to help people: 
    * It was moved specifically because Github would not cancel their ICE contract. The Immigration and Customs Enforcement agency was created in March 2003. 
    * This repository acknowledges non-Indigious people in the United States live on stolen land and that the United States has continually broken treaties with First Peoples/Native Americans/Indigeous North American peoples. Please [learn about the tribes](https://native-land.ca/) that were where you reside long before you. [Support water protectors](https://waterprotectorlegal.org/). Water is life.

## License 
This is licensed under GNU GPLv2. [Read what that means](https://choosealicense.com/licenses/gpl-2.0/)

##Archive
* [Ubuntu 15.10 Python Dev Env](ubuntu-15.10_dev_env.md) - Python dev env only in Ubuntu 15.10
* [Ubuntu 21.04 on my Framework Labtop](ubuntu-21.04_impish_on_framework.md)
